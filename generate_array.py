import cv2
import sys

header = open('map_array.h', 'w')
header.write('#pragma once\n'
             'int map_array[3][256] = {\n')

_map = cv2.imread(sys.argv[1])
rows, cols, channels = _map.shape

for i in range(rows):
    header.write('\t{')
    for j in range(cols):
        header.write('{},'.format(_map[i, j, channels - i - 1]))
    header.write('},\n')

header.write('};\n')
