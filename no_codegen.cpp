#include <opencv2/opencv.hpp>
#include <chrono>

using namespace cv;
int main(int argc, char** argv )
{
    if(argc != 4)
    {
        std::cout << "wrong amount of input args" << std::endl;
        return -1;
    }

    using namespace std::chrono;
    steady_clock::time_point time_point = steady_clock::now();

    Mat map = imread(argv[1]);
    Mat image = imread(argv[2]);

    for(int i = 0; i < image.rows; ++i) {
        for(int j = 0; j < image.cols; ++j) {
            auto& pixel = image.at<Vec3b>(i, j);
            for(int ch = 0; ch < image.channels(); ++ch) {
                // png stores channels in BGR order (wtf?!)
                pixel[ch] = map.at<Vec3b>(image.channels() - 1 - ch, pixel[ch])[ch];
            }
        }
    }

    imwrite(argv[3], image);

    auto duration = steady_clock::now() - time_point;
    std::cout << "Total duration is " << duration_cast<milliseconds>(duration).count() << "ms\n";

    return 0;
}