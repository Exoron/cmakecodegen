# BuildSystems
### Task4 - CMakeCodegen

Шаблон использования:
```bash
mkdir build
cd build
cmake ..
make

./NoCodegen ../map.png <input_image>.png <output_image>.png

./WithCodegen <input_image>.png <output_image>.png
```

В корневой директории есть тестовое изображение Lena.png, которое можно использовать