#include <opencv2/opencv.hpp>
#include "map_array.h"
#include <chrono>

using namespace cv;
int main(int argc, char** argv )
{
    if(argc != 3)
    {
        std::cout << "wrong amount of input args" << std::endl;
        return -1;
    }

    using namespace std::chrono;
    steady_clock::time_point time_point = steady_clock::now();

    Mat image = imread(argv[1]);

    for(int i = 0; i < image.rows; ++i) {
        for(int j = 0; j < image.cols; ++j) {
            auto& pixel = image.at<Vec3b>(i, j);
            for(int ch = 0; ch < image.channels(); ++ch) {
                pixel[ch] = map_array[ch][pixel[ch]];
            }
        }
    }

    imwrite(argv[2], image);

    auto duration = steady_clock::now() - time_point;
    std::cout << "Total duration is " << duration_cast<milliseconds>(duration).count() << "ms\n";

    return 0;
}